# Sample-PWA

## Reminder: Steps to build a simple PWA

- [ ] manifest.json
- [ ] service-worker.js
- [ ] icons for installation
- [ ] inspector analysis
- [ ] lighthouse report 

Open the sample-PWA in Chrome and try to install:

https://ui-programming-21-22.gitlab.io/sample-pwa
